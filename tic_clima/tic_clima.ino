#include <SPI.h>
#include <Ethernet.h>
#include <ThingerEthernet.h>
#include "DHT.h" //Sensor DHT

// Parámetros del dispositivo ESP8266
#define USERNAME "dacopancm"
#define DEVICE_ID "climauce"
#define DEVICE_CREDENTIAL "climauce"

// Parámetros de nuestra red wifi
#define wifi_ssid "ssid_red_wifi"
#define wifi_password "password_red_wifi"

//pines
#define dht_pin 2 // GPIO13
#define s_lluvia A0
#define UV_OUT A5
#define UV_REF A4

// Parámetros del sensor DHT11
#define dht_type DHT11 // Seleccionamos el sensor
DHT dht(dht_pin, dht_type);

ThingerEthernet thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);

void setup() {
  Serial.begin(9600);
  Serial.println("start setup");
  pinMode(UV_OUT, INPUT);
  pinMode(UV_REF, INPUT);
  //thing.add_wifi(wifi_ssid, wifi_password);
  thing["sensores"] >> [](pson & out) {
    out["dht_humedad"] = dht.readHumidity();
    out["dht_temperatura"] = dht.readTemperature() - 10;
    //Serial.println("out humedad");
    //lluvia
    int lluvia = analogRead(s_lluvia);
    lluvia = map(lluvia, 0, 1023, 100, 0);
    out["lluvia"] = lluvia > 30 ? "SI" : "NO";
    //Serial.println("out lluvia");
    // rayos UV
    out["uv"] = get_uv();
    out["uv_range"] = get_range(get_uv());
    //Serial.println("out uv");
  };
  Serial.println("end setup");
}

void loop() {
  //Serial.println('start loop');
  thing.handle();
  //Serial.println('end loop');
}

float get_uv() {
  int uvLevel = averageAnalogRead(UV_OUT);
  int refLevel = averageAnalogRead(UV_REF);

  //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
  float outputVoltage = 3.3 / refLevel * uvLevel;

  float uvIntensity = mapfloat(outputVoltage, 0.99, 2.8, 0.0, 15.0); //Convert the voltage to a UV intensity level
  return uvIntensity;
}


//Takes an average of readings on a given pin
//Returns the average
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0;

  for (int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;

  return (runningValue);
}

//The Arduino Map function but for floats
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

String get_range(float uv) {
  if (uv < 2) {
    return "bajo";
  } else if (uv < 5) {
    return "moderado";
  }
  else if (uv < 7) {
    return "alto";
  } else if (uv < 10) {
    return "muy moderado";
  } else {
    return "extremadamente alto";
  }
}
