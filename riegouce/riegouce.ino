/*
    This code should be pasted within the files where this function is needed.
    This function will not create any code conflicts.

    The function call is similar to printf: ardprintf("Test %d %s", 25, "string");
    To print the '%' character, use '%%'
    This code was first posted on http://arduino.stackexchange.com/a/201
*/

#ifndef ARDPRINTF
#define ARDPRINTF
#define ARDBUFFER 16 //Buffer for storing intermediate strings. Performance may vary depending on size.
#include <stdarg.h>
#include <Arduino.h> //To allow function to run from any file in a project

int ardprintf(char *str, ...) //Variadic Function
{
  int i, count = 0, j = 0, flag = 0;
  char temp[ARDBUFFER + 1];
  for (i = 0; str[i] != '\0'; i++)  if (str[i] == '%')  count++; //Evaluate number of arguments required to be printed

  va_list argv;
  va_start(argv, count);
  for (i = 0, j = 0; str[i] != '\0'; i++) //Iterate over formatting string
  {
    if (str[i] == '%')
    {
      //Clear buffer
      temp[j] = '\0';
      Serial.print(temp);
      j = 0;
      temp[0] = '\0';

      //Process argument
      switch (str[++i])
      {
        case 'd': Serial.print(va_arg(argv, int));
          break;
        case 'l': Serial.print(va_arg(argv, long));
          break;
        case 'f': Serial.print(va_arg(argv, double));
          break;
        case 'c': Serial.print((char)va_arg(argv, int));
          break;
        case 's': Serial.print(va_arg(argv, char *));
          break;
        default:  ;
      };
    }
    else
    {
      //Add to buffer
      temp[j] = str[i];
      j = (j + 1) % ARDBUFFER;
      if (j == 0) //If buffer is full, empty buffer.
      {
        temp[ARDBUFFER] = '\0';
        Serial.print(temp);
        temp[0] = '\0';
      }
    }
  };

  Serial.println(); //Print trailing newline
  return count + 1; //Return number of arguments detected
}

#undef ARDBUFFER
#endif

// by dacopanCM
#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>

#define USERNAME "dacopancm"
#define DEVICE_ID "riegouce"
#define DEVICE_CREDENTIAL "riegouce"

#define SSID "Iotriego"
#define SSID_PASSWORD "dacopan12345"

// Parámetros del sensor DHT11
#define humedad A0
#define bomba D7
#define lluvia D6

int p_humedad = 0;
bool autoencendido = false;
bool bombaOn = false;
long t_encendido = 0;
float agua = 0.11;

ThingerESP8266 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);

void setup() {

  thing.add_wifi(SSID, SSID_PASSWORD);

  thing["sensores"] >> [](pson & out) {
    //Serial.println("----> sensores read from azure -> ");
    out["dht_humedad"] = p_humedad;
    out["lluvia"] = digitalRead(lluvia) == LOW ? "SI" : "NO";
    out["bomba"] = bombaOn ? "ENCENDIDA" : "APAGADA";
    out["automatico"] = autoencendido ? "ACTIVO" : "INACTIVO";
  };

  thing["bomba"] << [](pson & in) {
    if (in.is_empty()) {
      in = digitalRead(bomba);
    }
    else {
      if (in) {
        encender_bomba();
      } else {
        apagar_bomba();
      }
      String inx = (in ? "ON" : "OFF");
      autoencendido = false;
      Serial.println("----> bomba -> " + inx);
    }
  };


  thing["automatico"] << [](pson & in) {
    if (in.is_empty()) {
      in = autoencendido;
    }
    else {
      autoencendido = in;
      String inx = (autoencendido ? "ON" : "OFF");
      Serial.println("----> autoencendido -> " + inx);
    }

  };

  //thing["agua"] << inputValue(agua);


  pinMode(humedad, INPUT);
  pinMode(bomba, OUTPUT);
  pinMode(lluvia, INPUT);

  digitalWrite(bomba, LOW);
  Serial.begin(9600);
  print_mac();
}

void loop() {
  /*variables humedad*/
  int range = analogRead(humedad);
  p_humedad = map(range, 0, 1023, 100, 0);
  p_humedad = p_humedad + 1;
  //Serial.println("\n___________________________________________________");
  //Serial.print("--porcentaje humedad -> ");
  //Serial.print(p_humedad);

  /*inicio control humedad*/
  if (p_humedad <= 30 && autoencendido)
  {
    digitalWrite(bomba, HIGH);
    encender_bomba();
    Serial.println("----> Suelo Seco -> Bomba Encendida");
  } else if (autoencendido) {
    Serial.println("----> Suelo Humedo -> Bomba Apagada");
    apagar_bomba();
    digitalWrite(bomba, LOW);
  }
  thing.handle();
  delay(1000);
}

void encender_bomba() {
  digitalWrite(bomba, HIGH);
  bombaOn = true;
  t_encendido = millis();
}

void apagar_bomba() {
  digitalWrite(bomba, LOW);
  bombaOn = false;

  long current_millis = millis();
  long t_delta = current_millis - t_encendido;
  t_delta = t_delta / 1000;
  t_encendido = current_millis;

  pson data;
  data["delta"] = t_delta;
  data["agua"] = (t_delta) * agua;
  thing.write_bucket("riego_hist_bomba", data);

  ardprintf("apagar_bomba: delta: %l  agua %f \n" , t_delta , agua);
}

void print_mac() {
  Serial.println();
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());
}
